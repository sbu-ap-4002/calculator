# Calculator:

Implementation of basic calculator to improve git skills

## Description:
In this exercise, you should complete a piece of code which represent a simple calculator.
Main purpose of this exercise is improving your git skills and java's part in not main target of this exercise.
In this project, you should complete four functions in file `Calculator.java`. You should create branch Develop and then on it four branches
for four functions `sumFunction`, `subFunction`, `divideFunction` and `productFunction` and complete each function
in its own branch. Your branches must have logical and recognizable names. In each branch, after you completed implementation you should write a message in `doc.txt` file.
The messages will be these: 
    
* For `sumFunction` : I implemented sumFunction!
* For `subFunction` : I implemented subFunction!
* For `divideFunction` : I implemented divideFunction!
* For `productFunction` : I implemented productFunction!

At the end your final task will be merging all these branches with Develop branch and fix some estimated conflicts.
After you merged all  branches with Develop branch, you should make a merge request to main branch on your own project.
Your TA will only evaluate your exercise with your merge request and git history so don't be so creative in this exercise.

Good Luck, Have Fun, Code a lot!
